#!/bin/bash
rm -rf /usr/java

wget -c -q "https://download.oracle.com/otn/java/jdk/8u221-b11/230deb18db3e4014bb8e3e8324f81b43/jdk-8u221-linux-x64.tar.gz?AuthParam=1569403613_fca46dc5b975a6e352842f6042ffaffc" -O /opt/jdk-8u221-linux-x64.tar.gz 
cd /opt && tar zxf jdk-8u221-linux-x64.tar.gz && mkdir /usr/java/ && mv /opt/jdk1.8.0_221 /usr/java && rm /opt/jdk-8u221-linux-x64.tar.gz
#Update alternatives section
update-alternatives --install /usr/bin/java java /usr/java/jdk1.8.0_221/jre/bin/java 20000
update-alternatives --install /usr/bin/jar jar /usr/java/jdk1.8.0_221/bin/jar 20000
update-alternatives --install /usr/bin/javac javac /usr/java/jdk1.8.0_221/bin/javac 20000
update-alternatives --install /usr/bin/javaws javaws /usr/java/jdk1.8.0_221/jre/bin/javaws 20000
update-alternatives --set java /usr/java/jdk1.8.0_221/jre/bin/java
update-alternatives --set javaws /usr/java/jdk1.8.0_221/jre/bin/javaws
update-alternatives --set javac /usr/java/jdk1.8.0_221/bin/javac
update-alternatives --set jar /usr/java/jdk1.8.0_221/bin/jar
#check version
java -version